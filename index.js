'use strict';

var assert = require('assert'),
	Sequelize = require('sequelize'),
	sequelize = new Sequelize('testsequelize', process.env['PGUSER'], process.env['PGPASS'], {
		dialect: "postgres",
		port: 5432,
	}),
	User = sequelize.define('User', {
		userId: { type: Sequelize.BIGINT, primaryKey: true, autoIncrement: true },
		username: { type: Sequelize.STRING, unique: true },
		password: Sequelize.STRING,
	});

// Generator handler

function isGeneratorFunction(f) {
	return typeof f === 'function' && f.constructor && f.constructor.name === 'GeneratorFunction';
}

function run(generator, cb, args) {
	// Run the generator if it hasn't been run yet.
	if (isGeneratorFunction(generator)) generator = generator();
	// If there is no yield, we have nothing to do
	if (typeof generator.next !== 'function') return;
	// Now we can call next and get the yielded value
	var f = generator.next.apply(generator, args || []);
	// When the generator has returned, f.done will be true
	if (f.done) {
		cb.call(this, f.value);
		return;
	}
	// Create a generic callback that can be used in all cases.
	var callback = function (err) {
		var args = Array.prototype.slice.call(arguments, 1);
		// In all cases we will have at least one argument which is a boolean so that yield can work
		if (!args.length) args.push(!err);
		else if (typeof args[0] === 'undefined') args[0] = !err;
		// Keep recursing until we're done
		process.nextTick(function () {
			if (err) return generator.throw(err);
			// Recurse on success
			run(generator, cb, args);
		});
	};
	// If a promise was yielded, we can provide the success and error functions.
	if (typeof f.value.then === 'function') {
		f.value.then(callback.bind({}, null), callback);
	} else if (typeof f.value.complete === 'function') {
		f.value.complete(callback);
	} else {
        // Assuming value is a function
		f.value(callback);
	}
}

// Main

run(function* () {
	var ok, t, u1, u2, u3;
	// Check that the database can be connected to
	ok = yield sequelize.authenticate();
	assert(ok, "Could not connect to database");
	// Sync database so that tables are available
	// NOTE: we use sync with force=true to recreate the tables on every run
	// In order to test the generator.throw system, you can remove the force true option
	// Then on second run of this script, User.create will throw an error.
	ok = yield sequelize.sync({ force: true });
	assert(ok, "Could not sync database");
	// Get a transaction
	t = yield sequelize.transaction();
	assert(t, "Could not get a transaction");
	// Build some users
	// Commit and done
	try {
		u1 = yield User.create({ username: 'oneuser' }, { transaction: t });
		u2 = yield User.create({ username: 'twouser' }, { transaction: t });
		u3 = yield User.create({ username: 'threeuser' }, { transaction: t });
		// Now we commit
		ok = false;
		ok = yield t.commit();
		assert(ok, "Could not commit transaction");
		// Print output
		[u1, u2, u3].forEach(function(u) {
			console.log("Created user with username = " + u.username);
		});
	} catch (err) {
		t.cleanup();
		console.log("An error occured");
	}
	return "All Done!";

}, (val) => {
    sequelize.close();
    console.log(val);
});
